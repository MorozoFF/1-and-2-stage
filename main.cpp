#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
#include <fstream>  
#include <stdio.h>

using namespace std;

int DEL(int** A, int X)
{
  for (int i = 0; i < X; i++)
    delete[] A[i];
     delete[] A;
  return 0;
}
int** FILL(int tou, int  bal) 
{
  int** B = new int*[tou];
  for (int i = 0; i < tou; i++) 
  {
    B[i] = new int[ bal];
  }
  for (int i = 0; i <tou; i++) 
  {
    for (int j = 0; j <  bal; j++)
    {
      B[i][j] = 0;
    }
  }
  return B;
  DEL(B, tou);
}
int OUT(int** C, int tou, int  bal) 
{
  for (int i = 0; i < tou; i++)
  {
    for (int j = 0; j <  bal; j++)
    {
      cout.width(5);
      cout << C[i][j];
    }
    cout << endl;
  }
  return 0;
}
int** TRANSP(int** C, int* tou, int*  bal)
{
  int** TRA = new int*[* bal];
  for (int i = 0; i < * bal; i++)
  {
    TRA[i] = new int[*tou];
  }
  for (int i = 0; i < * bal; i++)
  {
    for (int j = 0; j < *tou; j++) 
    {
      TRA[i][j] = C[j][i];
    }
  }

  DEL(C, *tou);
  *tou += * bal;
  *bal = *tou - *bal;
  *tou = *tou - *bal;
  return TRA;
}
int** SME(int f, int k, int l, int j, int* tou, int* bal, int** A)
{
  int** B = FILL(*tou + k + j, *bal + f + l);
  for (int i = 0; i < *tou; i++)
  {
    for (int p = 0; p < *bal; p++)
    {
      B[i + k][p + f] = A[i][p];
    }
  }
  DEL(A, *tou);
  *tou = *tou + k + j;
  *bal = *bal + f + l;
  return B;
  DEL(B, *tou);
}
int** UMN(int** A, int** B, int tou, int bal, int tou2, int bal2)
{
  int** MUL = FILL(tou, bal2);
  for (int i = 0; i < tou; i++)
  {
    for (int j = 0; j < bal2; j++) 
    {
      for (int k = 0; k < bal; k++) 
      {
        MUL[i][j] += A[i][k] * B[k][j];
      }
    }
  }
  return MUL;
  DEL(MUL, tou);
}
int OPR(int** A, int n) {
  int ert = 0;
  if (n == 2) {
    ert = A[0][0] * A[1][1] - A[0][1] * A[1][0];
  } else {
    for (int i = 0; i < n; i++)
    {
      int m1 = 1, m2 = 1;
      for (int j = 0; j < n; j++)
      {
        m1 *= A[(i + j) % n][j];
        m2 *= A[(j + i) % n][n - 1 - j];
      }
      ert = ert + m1 - m2;
    }
  }
  return ert;
}
int** OBR(int** A, int n) 
{
  int** O = FILL(n, n);
  int ert = OPR(A, n);
  for (int i = 0; i < n; i++) 
  {
    for (int j = 0; j < n; j++) 
    {
      int** B = FILL(n - 1, n - 1);
      int tou = 0, bal = 0;
      for (int l = 0; l < n; l++)
      {
        for (int m = 0; m < n; m++)
        {
          if ((l != i) && (m != j)) 
          {
            B[tou][bal] = A[l][m];

            bal++;
            if (bal == n - 1) {
              tou++;
              bal = 0;
            }
          }
        }
      }
      O[i][j] = OPR(B, n - 1) / ert;
      DEL(B, n - 1);

      if ((i + j) % 2 == 1) 
      {
        O[i][j] = -O[i][j];
      }
    }
  }
  DEL(A, n);
  O = TRANSP(O, &n, &n);
  return O;
  DEL(O, n);
}
int main(int argc, char* argv[])
{
  string s_menu, rzw, file;
  setlocale(LC_ALL, "RUS");
  int nom_argv = 2, emp_matrix = 0, exit = 0, k, cl, rw, num, cp;


  if (argc == 1)
    cout << "Нет параметров" << endl;
  else {
    int tou = atoi(argv[1]);
    char* p = strchr(argv[1], 'x');
    p++;
    int bal = atoi(p);
    if (argc < tou * bal + 2)
    {
      k = argc;
    } else {
      k = tou * bal + 2;
    }
    int** A = new int*[1];
    if ((tou != 0) && (bal != 0)) 
    {
      A = FILL(tou, bal);
    }

    int l = 0;
    int m = 0;
    int waz = 0;
    if (argc == 3) {
      string arg = argv[2];
      replace(arg.begin(), arg.end(), ',', ' ');
      while ((l < tou) && (waz < arg.size()))
      {
        A[l][m] = A[l][m] * 10 + arg[waz] - 48;
        waz++;
        if (arg[waz] == ' ')
        {
          waz++;
          m++;
          if (m == bal) {
            m = 0;
            l++;
          }
        }
      }
    } else if (argc == 2) 
    {
      cout << "Матрица пустая!\n";
      emp_matrix = 1;
    } else {
      for (int i = 0; i < tou; i++)
      {
        for (int j = 0; j < bal; j++)
        {
          if (nom_argv == k) {
            break;
          }
          A[i][j] = atoi(argv[nom_argv]);
          nom_argv++;
        }
      }
    }
    while (exit != 1) {
      cout << "1.The output matrix" << endl
      
           << "2.To fold the matrix" << endl
           
           << "3.To multiply the matrix" << endl
           
           << "4.To transpose a matrix" << endl
           
           << "5.To expand the matrix" << endl
           
           << "6.Find items" << endl
           
            << "7.To change the value of the item" << endl
            
           << "15.The output from the program" << endl;
      int n_menu = 0;
      cin >> n_menu;
      switch (n_menu)
      {
        case 1:
          if (emp_matrix == 1) 
          {
            cout << "The matrix is empty!\n";
          } else {
            OUT(A, tou, bal);
          }
          break;
        case 2:
          if (emp_matrix == 1)
          {
            cout << "The matrix is empty!\n";
          } else {
            int** B = FILL(tou, bal);
            cout << "Enter the matrix: " << tou << "x" << bal << endl;
            for (int i = 0; i < tou; i++)
            {
              for (int j = 0; j < bal; j++) 
              {
                cin >> B[i][j];
              }
            }
            for (int i = 0; i < tou; i++)
            {
              for (int j = 0; j < bal; j++)
              {
                cout.width(5);
                cout << A[i][j] + B[i][j] << " ";
              }
              cout << endl;
            }
            DEL(B, tou);
          }
          break;
        case 3:
          if (emp_matrix == 1)
          {
            cout << "The matrix is empty!\n";
          } else {
            cout << "Enter the size of the matrix: ";
            cin >> rzw;
            int tou2 = atoi(rzw.c_str());
            const char* y = strchr(rzw.c_str(), 'x');
            y++;
            int bal2 = atoi(y);
            if (bal != tou2) 
            {
              cout << "Wrong size" << endl;
            } else {
              int** B = FILL(tou2, bal2);
              for (int i = 0; i < tou2; i++) 
              {
                for (int j = 0; j < bal2; j++) 
                {
                  cin >> B[i][j];
                }
              }
              OUT(UMN(A, B, tou, bal, tou2, bal2), tou, bal2);
              DEL(B, tou2);
            }
          }
          break;
        case 4:
          if (emp_matrix == 1) 
          {
            cout << "The matrix is empty!\n";
          } else {
            A = TRANSP(A, &tou, &bal);
          }
          break;
        case 5:
          if (emp_matrix == 1)
          {
            cout << "The matrix is empty!\n";
          } else {
            char c;
            cout << "Select the direction" << endl
            
                 << "h left" << endl
                 
                 << "k up" << endl
                 
                 << "l right" << endl
                 
                 << "j down" << endl;
                 
            int h = 0, k = 0, l = 0, j = 0;
            cin >> c;
            switch (c) {
              case 'h':
                h = 1;
                break;
              case 'k':
                k = 1;
                break;
              case 'l':
                l = 1;
                break;
              case 'j':
                j = 1;
                break;
              default:
                cout << "Invalid command" << endl;
                break;
            }
            A = SME(h, k, l, j, &tou, &bal, A);
          }
          break;
        case 6:
          if (emp_matrix == 1) {
            cout << "The matrix is empty!\n";
          } else {
            cout << "Enter the value" << endl;
            int t = 0, zn;
            cin >> zn;
            for (int i = 0; i < tou; i++)
            {
              for (int j = 0; j < bal; j++)
              {
                if (A[i][j] == zn) {
                  t = 1;
                  printf("[%d][%d] ", i, j);
                }
              }
            }
            if (t == 0) {
              cout << "Element not found";
            }
            cout << endl;
          }
          break;
          case 7:
          if (emp_matrix == 1) {
            cout << "The matrix is empty!\n";
          } else {
            int I = 0, J = 0, izmen;
            cout << "Enter the position ([i][j]): ";
            cin >> rzw;
            I = atoi(rzw.c_str() + 1);
            J = atoi(rzw.c_str() + rzw.rfind('[') + 1);
            if ((I < 0) || (I >= tou) || (J < 0) || (J >= bal))
            {
              cout << "Incorrect position" << endl;
            } else {
              cout << "Enter a new value: ";
              cin >> izmen;
              cout << endl;
              A[I][J] = izmen;
            }
          }
          break;

        case 15:
          cout << "Are you sure you want to go? (y/N): ";
          cin >> s_menu;
          if ((s_menu == "y") || (s_menu == "Y") || (s_menu == "yes") ||
              (s_menu == "Yes") || (s_menu == "Yea"))
              {
            exit = 1;
          }
          break;
      }
    }
    if ((tou != 0) && (bal != 0))
    {
      DEL(A, tou);
    }
#ifdef WIN32
    system("pause");
#endif
  }
}